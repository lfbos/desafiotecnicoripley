# Despliegue en Heroku

* Crear cuenta en firebase, habilitando autenticación email/password, adicionalmente crear base de datos con una colección llamada **users**.

* Clonar proyecto

    `git clone https://github.com/lfbos/desafiotecnicoripley.git`

* Compilar frontend (ver página principal).

* Instalar cliente [heroku](https://devcenter.heroku.com/articles/heroku-cli#download-and-install).

* Iniciar sesión heroku

    `heroku login`

* Crear aplicación
    
    `heroku create appname`
    
* Habilitar redis

    `heroku addons:create heroku-redis:hobby-dev -a appname`
    
    Para ver la url de REDIS:
    `heroku config | grep REDIS`
    
* En la configuración de la aplicación (en la página de heroku) ir a **Settings** > **Reveal Config Vars**, agregar
las variables de entorno correspondientes (ver ./server/.env.example).

* Desplegar aplicación

    `git subtree push --prefix server heroku master`

* Abrir la aplicación `heroku open`

* Y listo!
