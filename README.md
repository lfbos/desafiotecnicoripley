# Desafío técnico Banco Ripley

Desafío creado en base a los requerimientos para el puesto de Desarrollador Full Stack en Banco Ripley.

Para ver la aplicación corriendo puede ingresar [aquí](https://ripleydt.herokuapp.com).

## Instalación para correr de forma local

* Clonar proyecto

    `git clone https://github.com/lfbos/desafiotecnicoripley.git`

### Frontend

* Instalar angular

    `npm install -g @angular/cli`
* Acceder a la carpeta `client`

    `cd client/`
* Instalar dependencias
    
    `npm i`
    
* Compilar proyecto
   
   `ng build --prod --outputPath=../server/src/front`
   
### Backend
* Crear cuenta en firebase, habilitando autenticación email/password, adicionalmente crear base de datos con una colección llamada **users**

* Instalar y correr [redis](https://redis.io/).

* Agregar las variables de entorno necesarias en un archivo `.env` (ver ./server/.env.example) 

* Acceder a la carpeta `server`

    `cd server/`
* Instalar dependencias
    
    `npm i`
    
* Iniciar aplicación
   
   `npm run dev` ir a [http://localhost:3000](http://localhost:3000)
   
* **Opcional**: correr pruebas `npm run test`

## Despliegue

Para detalles del despliegue de la aplicación en heroku ir a [deployment](https://github.com/lfbos/desafiotecnicoripley/blob/master/DEPLOYMENT.md).