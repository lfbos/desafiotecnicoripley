export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyB18RcRqyjc7PMjFQKFvQ-DHzxKSEtNwac",
    authDomain: "ripley-auth.firebaseapp.com",
    databaseURL: "https://ripley-auth.firebaseio.com",
    projectId: "ripley-auth",
    storageBucket: "ripley-auth.appspot.com",
    messagingSenderId: "978677907863",
    appId: "1:978677907863:web:ed0f8d8feee9daa0ea44d2",
    measurementId: "G-MZDFENSXLN"
  }
};
