// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB18RcRqyjc7PMjFQKFvQ-DHzxKSEtNwac",
    authDomain: "ripley-auth.firebaseapp.com",
    databaseURL: "https://ripley-auth.firebaseio.com",
    projectId: "ripley-auth",
    storageBucket: "ripley-auth.appspot.com",
    messagingSenderId: "978677907863",
    appId: "1:978677907863:web:ed0f8d8feee9daa0ea44d2",
    measurementId: "G-MZDFENSXLN"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
