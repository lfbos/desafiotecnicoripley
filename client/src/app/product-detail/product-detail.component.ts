import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {ApiService} from "../api.service";
import {Product} from "../product";

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  public loading = false;
  product: Product = null;
  visitedProducts: Product[] = [];
  longDescriptionExpanded = true;
  attrsExpanded = false;
  warrantiesExpanded = true;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.loading = true;
    const {params: {value: {partNumber}}}: any = this.route;

    this.apiService.getProductAndVisitedProducts(partNumber).subscribe((responses: any) => {
      this.product = responses[0];
      this.visitedProducts = responses[1];

      this.loading = false;
    }, error => {
      this.loading = false;
      this.apiService.handleError(error, this.router);
    });
  }

  toggleLongDescription() {
    this.longDescriptionExpanded = !this.longDescriptionExpanded;
  }

  toggleAttrs() {
    this.attrsExpanded = !this.attrsExpanded;
  }

  toggleWarranties() {
    this.warrantiesExpanded = !this.warrantiesExpanded;
  }
}
