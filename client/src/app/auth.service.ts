import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public afs: AngularFirestore
  ) {
  }

  signOut() {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signOut().then(() => {
        localStorage.removeItem('user');
        resolve();
      }).catch(() => reject());
    });
  }

  doRegister(data: any) {
    const {email, password, rut} = data;

    return new Promise<any>((resolve, reject) => {
      const users = this.afs.collection(
        'users', ref => ref.where('rut', '==', rut)
      ).get();

      users.subscribe(queriedItems => {
        if (queriedItems.size > 0) {
          reject({code: 'auth/rut-already-in-use'})
        } else {
          firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(res => {
              const userId = res.user.uid;

              const userData = {
                userId,
                email,
                rut,
                name: data.name,
                lastName: data.lastName
              };

              this.afs.collection('users').add(userData).then(r => {
                resolve(r);
              });
            }, err => reject(err))
        }
      });
    })
  }

  doLogin(email, password) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(res => {
          this.setUser(res, resolve, reject);
        }, err => reject(err));
    });
  }

  setUser(data, resolve, reject) {
    const users = this.afs.collection(
      'users', ref => ref.where('userId', '==', data.user.uid)
    ).get();

    users.subscribe(queriedItems => {
      if (queriedItems.size > 0) {
        let userData = queriedItems.docs[0].data();

        const userAuthInfo = JSON.parse(JSON.stringify(data.user));
        userData = {
          ...userData,
          ...userAuthInfo.stsTokenManager
        };

        localStorage.setItem('user', JSON.stringify(userData));
        resolve()
      } else {
        reject();
      }
    });
  }

  isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null;
  }

  getUser(){
    if (this.isLoggedIn()) {
      return JSON.parse(localStorage.getItem('user'));
    }

    return null;
  }

}
