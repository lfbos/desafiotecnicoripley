import {Component, OnInit} from '@angular/core';
import {ApiService} from "../api.service";
import {Product} from "../product";
import {Router} from "@angular/router";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  public loading = false;
  products: Product[] = [];

  constructor(
    private apiService: ApiService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.loading = true;

    this.apiService.getProducts()
      .subscribe(products => {
        this.products = products;
        this.loading = false;
      }, error => {
        this.loading = false;
        this.apiService.handleError(error, this.router);
      });
  }
}
