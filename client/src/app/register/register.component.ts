import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";

import {AuthService} from "../auth.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public loading = false;
  registerForm;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.registerForm = this.formBuilder.group({
      name: '',
      lastName: '',
      rut: '',
      email: '',
      password: '',
      repeatPassword: ''
    });
  }

  checkPasswords(group: FormGroup) {
    const password = group.get('password').value;
    const repeatPassword = group.get('repeatPassword').value;

    return password === repeatPassword ? null : {passwordDoesNotMatch: true};
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: new FormControl(
        this.registerForm.name, [
          Validators.required
        ]
      ),
      lastName: new FormControl(
        this.registerForm.lastName, [
          Validators.required
        ]
      ),
      rut: new FormControl(
        this.registerForm.rut, [
          Validators.required,
          Validators.pattern(/\b(\d{1,3}(?:\d{3}){2}(-)[\dkK])\b/)
        ]
      ),
      email: new FormControl(
        this.registerForm.email, [
          Validators.required,
          Validators.email
        ]
      ),
      password: new FormControl(
        this.registerForm.password, [
          Validators.required
        ]
      ),
      repeatPassword: new FormControl(
        this.registerForm.repeatPassword, [
          Validators.required,
        ]
      )
    }, {
      validator: this.checkPasswords
    });
  }

  register(data) {
    if (this.registerForm.valid && !this.loading) {
      this.loading = true;
      this.authService.doRegister(data)
        .then(() => {
          this.loading = false;
          this.registerForm.reset();
          this.router.navigate(['/login']);
        })
        .catch(e => {
          if (e.hasOwnProperty('code')) {
            if (e.code === "auth/weak-password") {
              this.registerForm.setErrors({registerError: 'La contraseña debe tener al menos 6 caracteres'});
            } else if (e.code === 'auth/email-already-in-use') {
              this.registerForm.setErrors({registerError: 'La dirección de correo electrónico ya está en uso por otra cuenta'});
            } else if (e.code === 'auth/rut-already-in-use') {
              this.registerForm.setErrors({registerError: 'El RUT ya está en uso'});
            }
          }

          this.loading = false;
        });
    }
  }

  get name() {
    return this.registerForm.get('name');
  }

  get lastName() {
    return this.registerForm.get('lastName');
  }

  get rut() {
    return this.registerForm.get('rut');
  }

  get email() {
    return this.registerForm.get('email');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get repeatPassword() {
    return this.registerForm.get('repeatPassword');
  }
}
