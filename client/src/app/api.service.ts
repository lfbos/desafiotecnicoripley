import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "./product";
import {AuthService} from "./auth.service";
import {forkJoin} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiURL: string = "/api/v1";

  constructor(
    public httpClient: HttpClient,
    private authService: AuthService
  ) {
  }

  public handleError(error, router) {
    if (error.error && error.error.code) {
      if (error.error.code === "auth/id-token-expired") {
        this.authService.signOut().then(() => {
          router.data = {message: 'Sesión expirada'};
          router.navigate(['/login']);
        });
      }
    }
  }

  public getProducts() {
    const user = this.authService.getUser();

    return this.httpClient.get<Product[]>(`${this.apiURL}/products`, {
      headers: {
        Authorization: `Bearer ${user.accessToken}`
      }
    });
  }

  public getProductAndVisitedProducts(partNumber) {
    const user = this.authService.getUser();

    const productRequest = this.httpClient.get<Product>(`${this.apiURL}/products/${partNumber}`, {
      headers: {
        Authorization: `Bearer ${user.accessToken}`
      }
    });

    const visitedProducts = this.httpClient.get<Product>(`${this.apiURL}/visited-products`, {
      headers: {
        Authorization: `Bearer ${user.accessToken}`
      }
    });

    return forkJoin([productRequest, visitedProducts]);
  }
}
