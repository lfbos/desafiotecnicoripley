import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule} from "@angular/forms";

import {AngularFireModule} from "@angular/fire";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {HttpClientModule} from "@angular/common/http";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";

import {NgxLoadingModule} from 'ngx-loading';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavbarComponent} from './navbar/navbar.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ProductsComponent} from './products/products.component';
import {ProductDetailComponent} from './product-detail/product-detail.component';
import {environment} from "../environments/environment";
import {AuthNotRequired, AuthRequired} from "./activate.handlers";

const appRoutes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    canActivate: [AuthRequired]
  },
  {
    path: 'products/:partNumber',
    component: ProductDetailComponent,
    canActivate: [AuthRequired]
  },
  {
    path: 'login', component: LoginComponent,
    canActivate: [AuthNotRequired]
  },
  {
    path: 'register', component: RegisterComponent,
    canActivate: [AuthNotRequired]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    ProductsComponent,
    ProductDetailComponent
  ],
  imports: [
    NgxLoadingModule.forRoot({}),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy}, AuthRequired, AuthNotRequired
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
