export class Product {
  uniqueID: string;
  partNumber: string;
  name: string;
  locals: ListsOrLocals;
  fullImage: string;
  images?: (string)[] | null;
  prices: Prices;
  shortDescription: string;
  longDescription: string;
  definingAttributes?: (null)[] | null;
  attributes?: (AttributesEntity)[] | null;
  shipping: Shipping;
  xCatEntryCategory: string;
  productString: string;
  SKUs?: (SKUsEntity)[] | null;
  isMarketplaceProduct: boolean;
  marketplace: Marketplace;
  warranties?: (null)[] | null;
  url: string;
  thumbnailImage: string;
  simple: Simple;
  single: boolean;
}
class ListsOrLocals {
  outOfStockList: OutOfStockList;
  unavailableList: UnavailableList;
  promotion: Promotion;
}
class OutOfStockList {
  childrenXCatEntryQtyZero: boolean;
  xCatEntryQuantity: boolean;
  blacklist: boolean;
  noIMSDeliveries: boolean;
}
class UnavailableList {
  blacklist: boolean;
}
class Promotion {
  stock: string;
  badge: string;
}
class Prices {
  offerPrice: number;
  listPrice: number;
  cardPrice: number;
  discount: number;
  discountPercentage: number;
  ripleyPuntos: number;
  $$cache: $$cache;
  formattedOfferPrice: string;
  formattedListPrice: string;
  formattedCardPrice: string;
  formattedDiscount: string;
}
class $$cache {
  cached: boolean;
  created: number;
}
class AttributesEntity {
  displayable: boolean;
  name: string;
  usage: string;
  value: string;
  id?: string | null;
  identifier?: string | null;
}
class Shipping {
  rTienda: boolean;
  dDomicilio: boolean;
  rCercano: boolean;
  cashOnDelivery: boolean;
}
class SKUsEntity {
  Price?: (PriceEntity)[] | null;
  SKUUniqueID: string;
  partNumber: string;
  xCatEntryQuantity: number;
  ineligible: boolean;
  Attributes?: (null)[] | null;
  isMainProduct: boolean;
}
class PriceEntity {
  SKUPriceDescription: string;
  SKUPriceValue: string;
  SKUPriceUsage: string;
}
class Marketplace {
}
class Simple {
  lists: ListsOrLocals;
  isUnavailable: boolean;
  isOutOfStock: boolean;
  isMarketplaceProduct: boolean;
}
