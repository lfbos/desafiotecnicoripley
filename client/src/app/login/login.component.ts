import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {Router} from "@angular/router";

import {AuthService} from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loading = false;
  loginForm;
  message = null;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    });
  }

  ngOnInit() {
    const {data = null}: any = this.router;

    if (data && data.message) {
      this.message = data.message;

      setTimeout(() => {
        this.message = null;
      }, 2000);
    }

    this.loginForm = this.formBuilder.group({
      email: new FormControl(
        this.loginForm.email, [
          Validators.required,
          Validators.email
        ]
      ),
      password: new FormControl(
        this.loginForm.password, [
          Validators.required
        ]
      )
    });
  }

  login(data) {
    if (this.loginForm.valid && !this.loading) {
      this.loading = true;
      this.authService.doLogin(data.email, data.password)
        .then(() => {
          this.loading = false;
          this.router.navigate(['']);
        })
        .catch(() => {
          this.loading = false;
          this.loginForm.setErrors({
            loginError: 'Credenciales inválidas, por favor verifique'
          });
        });
    }
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }
}
