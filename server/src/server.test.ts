import supertest from "supertest";
import dotenv from 'dotenv';
import firebase from 'firebase';
import app from './server';

dotenv.config();

let request = supertest(app);

let firebaseConnection = firebase.initializeApp({
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DB_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MEASUREMENT_ID,
    appId: process.env.FIREBASE_APP_ID,
    measurementId: process.env.FIREBASE_MEASUREMENT_ID
});

const emailTestUser: any = process.env.EMAIL_TEST_USER;
const passwordTestUser: any = process.env.EMAIL_TEST_PASSWORD;

let accessToken: any = null;

const getAccessToken = () => {
    return new Promise<any>((resolve, reject) => {
        if (accessToken !== null) {
            resolve(accessToken);
        }

        firebaseConnection
            .auth()
            .signInWithEmailAndPassword(emailTestUser, passwordTestUser)
            .then((res: any) => {
                resolve(res);
            })
            .catch(() => reject());
    });
};

describe('Get Products', () => {
    it('should get 403 error /api/v1/products', done => {
        request
            .get('/api/v1/products')
            .expect(403)
            .end((err: any, res: any) => {
                if (err) throw err;

                done();
            });
    });

    it('should get 403 error /api/v1/products/1234', done => {
        request
            .get('/api/v1/products/1234')
            .expect(403)
            .end((err: any, res: any) => {
                if (err) throw err;

                done();
            });
    });

    it('should get 403 error /api/v1/visited-products', done => {
        request
            .get('/api/v1/visited-products')
            .expect(403)
            .end((err: any, res: any) => {
                if (err) throw err;

                done();
            });
    });

    it('should get 200 /api/v1/products', done => {
        getAccessToken()
            .then(res => {
                const userAuthInfo = JSON.parse(JSON.stringify(res.user));
                accessToken = userAuthInfo.stsTokenManager.accessToken;

                request
                    .get('/api/v1/products')
                    .set('Authorization', `Bearer ${accessToken}`)
                    .expect(200)
                    .end((err: any, res: any) => {
                        if (err) throw err;

                        done();
                    });
            }).catch((err) => done(err));
    });

    it('should get 200 /api/v1/visited-products', done => {
        getAccessToken()
            .then(res => {
                if (accessToken === null) {
                    const userAuthInfo = JSON.parse(JSON.stringify(res.user));
                    accessToken = userAuthInfo.stsTokenManager.accessToken;
                }

                request
                    .get('/api/v1/visited-products')
                    .set('Authorization', `Bearer ${accessToken}`)
                    .expect(200)
                    .end((err: any, res: any) => {
                        if (err) throw err;

                        done();
                    });
            }).catch((err) => done(err));
    });

    it('should get 200 /api/v1/products/2000371739194P', done => {
        getAccessToken()
            .then(res => {
                if (accessToken === null) {
                    const userAuthInfo = JSON.parse(JSON.stringify(res.user));
                    accessToken = userAuthInfo.stsTokenManager.accessToken;
                }

                request
                    .get('/api/v1/products/2000371739194P')
                    .set('Authorization', `Bearer ${accessToken}`)
                    .expect(200)
                    .end((err: any, res: any) => {
                        if (err) throw err;

                        done();
                    });
            }).catch((err) => done(err));
    });
});