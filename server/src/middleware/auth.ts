import {NextFunction, Request, Response} from "express";
import firebase from "../firebase";

export const isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
    if (!req.headers.authorization) {
        res.status(403).send('Credentials were not provided');
    }

    const auth: string | undefined = req.headers.authorization;
    let token: any = auth !== undefined ? auth.split('Bearer ') : null;

    if (token === null) {
        res.status(403).send('Credentials were not provided');
    }

    if (token.length === 0) {
        res.status(403).send('Invalid credentials');
    }

    token = token[1];

    firebase.auth().verifyIdToken(token)
        .then(() => {
            next();
        })
        .catch(err => {
            res.status(403).send(err);
        });
};