import {getProduct, getProductList, PRODUCTS} from "../redis";
import {Request, Response, NextFunction} from "express";

export const getCacheData = (key?: string) => (req: Request, res: Response, next: NextFunction) => {
    if (key === PRODUCTS) {
        return getProductList()
            .then((result: any) => {
                const products = JSON.parse(result);
                res.send(products)
            })
            .catch(() => next());
    }

    const {params: {partNumber}} = req;

    if (!partNumber) {
        next();
        return null;
    }

    return getProduct(partNumber)
        .then((result: any) => {
            const product = JSON.parse(result);
            res.send(product);
        })
        .catch(() => next());
};
