import * as firebase from 'firebase-admin';
import dotenv from 'dotenv';

dotenv.config();

export default firebase.initializeApp({
    databaseURL: process.env.FIREBASE_DB_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET
});