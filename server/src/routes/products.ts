import path from "path";
import {Request, Response} from "express";
import partNumbers from "../data/partNumbers";
import {getCacheData} from "../middleware/cache";
import {getVisitedProducts, PRODUCTS, saveProductList, saveVisitedProduct} from "../redis";
import {retryer} from "../utils/retryer";
import {isAuthenticated} from "../middleware/auth";

const baseURL: string = "https://simple.ripley.cl/api/v2/products";

export default [
    {
        path: '/',
        method: 'get',
        handler: [
            (req: Request, res: Response) => {
                res.sendFile(path.join(__dirname, '../front/index.html'));
            }
        ]
    },
    {
        path: "/api/v1/products",
        method: "get",
        handler: [
            isAuthenticated,
            getCacheData(PRODUCTS),
            async ({query}: Request, res: Response) => {
                try {
                    const url = `${baseURL}/?partNumbers=${partNumbers.join(',')}`;
                    const products: any = await retryer(url);

                    saveProductList(products);

                    res.status(200).send(products);
                } catch (e) {
                    res.status(500).send("Internal server error");
                }
            }
        ]
    },
    {
        path: "/api/v1/products/:partNumber",
        method: "get",
        handler: [
            isAuthenticated,
            getCacheData(),
            async ({params, headers, connection}: Request, res: Response) => {
                const {partNumber} = params;

                const url = `${baseURL}/${partNumber}`;

                try {
                    const product: any = await retryer(url);

                    const ip: any = headers['x-forwarded-for'] || connection.remoteAddress;

                    saveVisitedProduct(ip, product);

                    res.status(200).send(product);
                } catch (e) {
                    if (e.response.status === 404) {
                        res.status(404).send("Product not found");
                    } else {
                        res.status(500).send("Internal server error");
                    }
                }
            }
        ]
    },
    {
        path: "/api/v1/visited-products",
        method: "get",
        handler: [
            isAuthenticated,
            async ({headers, connection}: Request, res: Response) => {
                let products: any = [];

                try {
                    const ip: any = headers['x-forwarded-for'] || connection.remoteAddress;

                    const productsData: any = await getVisitedProducts(ip);
                    const productsRepeated: string[] = [];

                    productsData.forEach((p: string) => {
                        const product = JSON.parse(p);

                        if (productsRepeated.indexOf(product.partNumber) === -1) {
                            products.push(product);
                            productsRepeated.push(product.partNumber);
                        }
                    })
                } catch (e) {
                    /* empty */
                }

                res.status(200).send(products);
            }
        ]
    }
];
