import express from "express";
import http from "http";
import {applyMiddleware, applyRoutes} from "./utils";
import middleware from "./middleware";
import routes from './routes';

const router = express();

router.use(express.static(`${__dirname}/front`));

applyMiddleware(middleware, router);
applyRoutes(routes, router);

const {PORT = 3000, NODE_ENV} = process.env;
const server = http.createServer(router);

if (NODE_ENV !== 'test') {
    server.listen(PORT, () =>
        console.log(`Server is running http://localhost:${PORT}...`)
    );
}

export default router;