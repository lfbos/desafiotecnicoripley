import redis, {RedisClient} from 'redis';

const MAX_VISITED_PRODUCTS = 5;

const {
    REDIS_URL = 'redis://localhost:6379',
} = process.env;

const TTL = 120;

export const PRODUCTS = 'PRODUCTS';

export const client: RedisClient = redis.createClient(REDIS_URL);

client.on('connect', () => {
    console.log('Connected to redis client')
});

export const saveProductList = (products: Array<Object>) => {
    client.set(PRODUCTS, JSON.stringify(products));
    client.expire(PRODUCTS, TTL);

    products.forEach((product: any) => {
        const key = product.partNumber;
        client.set(key, JSON.stringify(product));
        client.expire(key, TTL);
    });
};

export const getProductList = () => new Promise((resolve, reject) => {
    client.get(PRODUCTS, (error, result) => {
        if (error || !result) {
            return reject();
        }

        return resolve(result);
    });
});

export const getProduct = (partNumber: string) => new Promise((resolve, reject) => {
    client.get(partNumber, (error, result) => {
        if (error || !result) {
            return reject();
        }

        return resolve(result);
    })
});

export const saveVisitedProduct = (ip: string, product: Object) => {
    const key = ip;
    const value = JSON.stringify(product);
    client.lrem(key, 0, value, () => {
        client.lpush(key, value, () => {
            client.ltrim(key, 0, MAX_VISITED_PRODUCTS - 1);
        });
    });
};

export const getVisitedProducts = (ip: string) => new Promise((resolve, reject) => {
    client.lrange(ip, 0, MAX_VISITED_PRODUCTS, (error, result) => {
        if (error || !result) {
            return reject();
        }

        return resolve(result);
    });
});
