import axios from "axios";

const ERROR_RATE = 15;

// Method to fake error and retry request
const shouldThrowFakeError = () => Math.floor(Math.random() * 100) <= ERROR_RATE;

export const retryer = (url: string) => new Promise(((resolve, reject) => {
    axios.get(url)
        .then(response => {
            if (shouldThrowFakeError()) {
                return resolve(retryer(url));
            }

            return resolve(response.data);
        }).catch((e) => reject(e));
}));