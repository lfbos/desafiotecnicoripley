"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redis_1 = require("../redis");
exports.getCacheData = function (key) { return function (req, res, next) {
    if (key === redis_1.PRODUCTS) {
        return redis_1.getProductList()
            .then(function (result) {
            var products = JSON.parse(result);
            res.send(products);
        })
            .catch(function () { return next(); });
    }
    var partNumber = req.params.partNumber;
    if (!partNumber) {
        next();
        return null;
    }
    return redis_1.getProduct(partNumber)
        .then(function (result) {
        var product = JSON.parse(result);
        res.send(product);
    })
        .catch(function () { return next(); });
}; };
