"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var firebase_1 = __importDefault(require("../firebase"));
exports.isAuthenticated = function (req, res, next) {
    if (!req.headers.authorization) {
        res.status(403).send('Credentials were not provided');
    }
    var auth = req.headers.authorization;
    var token = auth !== undefined ? auth.split('Bearer ') : null;
    if (token === null) {
        res.status(403).send('Credentials were not provided');
    }
    if (token.length === 0) {
        res.status(403).send('Invalid credentials');
    }
    token = token[1];
    firebase_1.default.auth().verifyIdToken(token)
        .then(function () {
        next();
    })
        .catch(function (err) {
        res.status(403).send(err);
    });
};
