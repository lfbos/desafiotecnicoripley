"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var redis_1 = __importDefault(require("redis"));
var MAX_VISITED_PRODUCTS = 5;
var _a = process.env.REDIS_URL, REDIS_URL = _a === void 0 ? 'redis://localhost:6379' : _a;
var TTL = 120;
exports.PRODUCTS = 'PRODUCTS';
exports.client = redis_1.default.createClient(REDIS_URL);
exports.client.on('connect', function () {
    console.log('Connected to redis client');
});
exports.saveProductList = function (products) {
    exports.client.set(exports.PRODUCTS, JSON.stringify(products));
    exports.client.expire(exports.PRODUCTS, TTL);
    products.forEach(function (product) {
        var key = product.partNumber;
        exports.client.set(key, JSON.stringify(product));
        exports.client.expire(key, TTL);
    });
};
exports.getProductList = function () { return new Promise(function (resolve, reject) {
    exports.client.get(exports.PRODUCTS, function (error, result) {
        if (error || !result) {
            return reject();
        }
        return resolve(result);
    });
}); };
exports.getProduct = function (partNumber) { return new Promise(function (resolve, reject) {
    exports.client.get(partNumber, function (error, result) {
        if (error || !result) {
            return reject();
        }
        return resolve(result);
    });
}); };
exports.saveVisitedProduct = function (ip, product) {
    var key = ip;
    var value = JSON.stringify(product);
    exports.client.lrem(key, 0, value, function () {
        exports.client.lpush(key, value, function () {
            exports.client.ltrim(key, 0, MAX_VISITED_PRODUCTS - 1);
        });
    });
};
exports.getVisitedProducts = function (ip) { return new Promise(function (resolve, reject) {
    exports.client.lrange(ip, 0, MAX_VISITED_PRODUCTS, function (error, result) {
        if (error || !result) {
            return reject();
        }
        return resolve(result);
    });
}); };
