"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var ERROR_RATE = 15;
// Method to fake error and retry request
var shouldThrowFakeError = function () { return Math.floor(Math.random() * 100) <= ERROR_RATE; };
exports.retryer = function (url) { return new Promise((function (resolve, reject) {
    axios_1.default.get(url)
        .then(function (response) {
        if (shouldThrowFakeError()) {
            return resolve(exports.retryer(url));
        }
        return resolve(response.data);
    }).catch(function (e) { return reject(e); });
})); };
