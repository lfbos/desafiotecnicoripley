"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var http_1 = __importDefault(require("http"));
var utils_1 = require("./utils");
var middleware_1 = __importDefault(require("./middleware"));
var routes_1 = __importDefault(require("./routes"));
var router = express_1.default();
router.use(express_1.default.static(__dirname + "/front"));
utils_1.applyMiddleware(middleware_1.default, router);
utils_1.applyRoutes(routes_1.default, router);
var _a = process.env, _b = _a.PORT, PORT = _b === void 0 ? 3000 : _b, NODE_ENV = _a.NODE_ENV;
var server = http_1.default.createServer(router);
if (NODE_ENV !== 'test') {
    server.listen(PORT, function () {
        return console.log("Server is running http://localhost:" + PORT + "...");
    });
}
exports.default = router;
