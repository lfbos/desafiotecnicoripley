"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var supertest_1 = __importDefault(require("supertest"));
var dotenv_1 = __importDefault(require("dotenv"));
var firebase_1 = __importDefault(require("firebase"));
var server_1 = __importDefault(require("./server"));
dotenv_1.default.config();
var request = supertest_1.default(server_1.default);
var firebaseConnection = firebase_1.default.initializeApp({
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DB_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MEASUREMENT_ID,
    appId: process.env.FIREBASE_APP_ID,
    measurementId: process.env.FIREBASE_MEASUREMENT_ID
});
var emailTestUser = process.env.EMAIL_TEST_USER;
var passwordTestUser = process.env.EMAIL_TEST_PASSWORD;
var accessToken = null;
var getAccessToken = function () {
    return new Promise(function (resolve, reject) {
        if (accessToken !== null) {
            resolve(accessToken);
        }
        firebaseConnection
            .auth()
            .signInWithEmailAndPassword(emailTestUser, passwordTestUser)
            .then(function (res) {
            resolve(res);
        })
            .catch(function () { return reject(); });
    });
};
describe('Get Products', function () {
    it('should get 403 error /api/v1/products', function (done) {
        request
            .get('/api/v1/products')
            .expect(403)
            .end(function (err, res) {
            if (err)
                throw err;
            done();
        });
    });
    it('should get 403 error /api/v1/products/1234', function (done) {
        request
            .get('/api/v1/products/1234')
            .expect(403)
            .end(function (err, res) {
            if (err)
                throw err;
            done();
        });
    });
    it('should get 403 error /api/v1/visited-products', function (done) {
        request
            .get('/api/v1/visited-products')
            .expect(403)
            .end(function (err, res) {
            if (err)
                throw err;
            done();
        });
    });
    it('should get 200 /api/v1/products', function (done) {
        getAccessToken()
            .then(function (res) {
            var userAuthInfo = JSON.parse(JSON.stringify(res.user));
            accessToken = userAuthInfo.stsTokenManager.accessToken;
            request
                .get('/api/v1/products')
                .set('Authorization', "Bearer " + accessToken)
                .expect(200)
                .end(function (err, res) {
                if (err)
                    throw err;
                done();
            });
        }).catch(function (err) { return done(err); });
    });
    it('should get 200 /api/v1/visited-products', function (done) {
        getAccessToken()
            .then(function (res) {
            if (accessToken === null) {
                var userAuthInfo = JSON.parse(JSON.stringify(res.user));
                accessToken = userAuthInfo.stsTokenManager.accessToken;
            }
            request
                .get('/api/v1/visited-products')
                .set('Authorization', "Bearer " + accessToken)
                .expect(200)
                .end(function (err, res) {
                if (err)
                    throw err;
                done();
            });
        }).catch(function (err) { return done(err); });
    });
    it('should get 200 /api/v1/products/2000371739194P', function (done) {
        getAccessToken()
            .then(function (res) {
            if (accessToken === null) {
                var userAuthInfo = JSON.parse(JSON.stringify(res.user));
                accessToken = userAuthInfo.stsTokenManager.accessToken;
            }
            request
                .get('/api/v1/products/2000371739194P')
                .set('Authorization', "Bearer " + accessToken)
                .expect(200)
                .end(function (err, res) {
                if (err)
                    throw err;
                done();
            });
        }).catch(function (err) { return done(err); });
    });
});
